document.addEventListener("DOMContentLoaded", function () {
    let questionActuelleIndex = 0; // Indice de la question actuelle
    const questions = listeQuestions.questions;  // Variable pour recuperer les questions du JSon
    let score = 0;
    displayQuestion();

    // Fonction pour Afficher la question, les réponses et l'image
    function displayQuestion() {
        let questionActuelle = questions[questionActuelleIndex];
        let questionQuizz = document.getElementById("question-quizz");
        let imageQuizz = document.getElementById("image-quizz");
        let reponseButtons = document.querySelectorAll('.reponse');
        let scoreContainer = document.getElementById("score-container");

        // Mettre à jour les questions la haut
        scoreContainer.textContent = `Question ${questionActuelleIndex + 1} / 20`;

        // Afficher la question
        questionQuizz.textContent = questionActuelle.question;

        //Afficher l'image
        imageQuizz.src = questionActuelle.image;
        imageQuizz.alt = "Image de la question";

        // Afficher les réponses dans les boutons
        reponseButtons.forEach((button, index) => {
            button.textContent = questionActuelle.options[index];
        });

        // Ajouter un écouteur d'évenement à chaque bouton de réponse
        reponseButtons.forEach(button => {
            button.addEventListener('click', handleReponseClick);
        });
    }

    // FOnction pour gérer le clic sur une réponse
    function handleReponseClick(event) {
        let selectionReponse = event.target.textContent;
        let questionActuelle = questions[questionActuelleIndex];

        // Verifier si la réponse est correcte
        if (selectionReponse === questionActuelle.bonneReponse) {
            score++;
            //BOnne reponse, reponse devient verte
            event.target.style.backgroundColor = "lightgreen";
        } else {
            // La réponse est incorrecte, changer la couleur du bouton en rouge
            event.target.style.backgroundColor = "lightcoral";
        }

        // Desactiver les boutons après avoir répondu 
        disableReponseButtons();

        // Afficher le bouton "Question Suivante"
        displayNextQuestionButton();
    }

    // Fonction pour désactiver les boutons de réponse
    function disableReponseButtons() {
        let reponseButtons = document.querySelectorAll('.reponse');
        reponseButtons.forEach(button => {
            button.disabled = true;
        });
    }

    // Fonction pour afficher le bouton "Question suivate "
    function displayNextQuestionButton() {
        let containerQuizz = document.getElementById("container-quizz");
        let nextQuestionButton = document.createElement('button');
        nextQuestionButton.textContent = 'Question suivante';
        nextQuestionButton.addEventListener('click', handleNextQuestionClick);
        containerQuizz.appendChild(nextQuestionButton);
        nextQuestionButton.id="nextQuestion";
    }

    // Fonction pour gérer le clic sur le bouton "Question suivante"
    function handleNextQuestionClick() {
        // Reinitialiser la couleur des boutons
        resetReponseButtonsColor();

        // Activer les boutons de réponse
        enableReponseButtons();

        // Supprimer le bouton "Question suivante"
        removeNextQuestionButton();

        // Passer à la question suivante
        questionActuelleIndex++;

        // Vérifier s'il y'a encore des questions
        if (questionActuelleIndex < questions.length) {
            // Afficher la prochaine question
            displayQuestion();
        } else {
            // Toutes les questions ont été répondues
            displayResultatModal();
        }
    }

    // Fonction pour réinitialiser la couleur des boutons de réponse
    function resetReponseButtonsColor() {
        let reponseButtons = document.querySelectorAll('.reponse');
        reponseButtons.forEach(button => {
            button.style.backgroundColor = 'white';
        });
    }

    // Fonction pour activer les boutons de réponse 
    function enableReponseButtons() {
        let reponseButtons = document.querySelectorAll('.reponse');
        reponseButtons.forEach(button => {
            button.disabled = false;
        });
    }

    // Fonction pour supprimer le bouton "Question suivante"
    function removeNextQuestionButton() {
        let containerQuizz = document.getElementById("container-quizz");
        let nextQuestionButton = containerQuizz.querySelector('#nextQuestion');
        if (nextQuestionButton) {
            containerQuizz.removeChild(nextQuestionButton);
        }
    }

    function displayResultatModal() {
        // Créer la fenêtre modale
        let modal = document.createElement('div');
        modal.classList.add('modal');

        // Ajouter le contenu
        modal.innerHTML = `
        <h2> Résultat du Quizz</h2>
        <p> Vous avez eu ${score} bonnes réponses sur 20.</p>`;

        // Overlay
        let overlay = document.createElement('div');
        overlay.classList.add('overlay');

        // Ajouter la fenêtre
        document.body.appendChild(modal);
        document.body.appendChild(overlay);

    }


});